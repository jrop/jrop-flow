/// <reference lib="dom" />

import { useRef, useState } from "react";
import { createRoot } from "react-dom/client";

import { cx } from "@emotion/css";
import { css } from "@emotion/react";

import { Flow, parallel, series, unit } from "../flow";
import { makeBubblesComponent } from "../ui";

const Unit_style = css`
  width: 150px;
  padding: 3px;
  margin: 3px;
  border: solid 1px black;
  border-radius: 1000px;

  &.ready {
    background-color: #eee;
    box-shadow: 0 0 5px rgba(0, 0, 0, 0.5);
  }

  &.pending {
    background-color: #eee;
  }

  &.running {
    background-color: #ddd;
  }

  &.success {
    background-color: #afa;
  }

  &.failure {
    background-color: #faa;
  }
`;

const Bubbles = makeBubblesComponent<number>({
  unitRenderer: (task) => (
    <button
      type="button"
      css={Unit_style}
      className={cx(task.status.kind, task.ready && "ready")}
    >
      {task.data}
    </button>
  ),
});

const App = () => {
  const flow = useRef(
    new Flow(
      series(
        unit(1),
        parallel(unit(2), unit(3)),
        unit(4),
        parallel(unit(5), unit(6)),
        unit(7),
      ),
    ),
  ).current;
  const [, setCount] = useState(0);
  const rerender = () => setCount((c) => c + 1);

  const runNext = () => {
    for (const ready of flow.nextReady()) {
      ready.status = { kind: "running" };
      rerender();

      setTimeout(
        () => {
          flow.reportDone(ready);
          rerender();
        },
        Math.random() * 1000 + 500,
      );
    }
  };

  return (
    <div>
      <h1>Flow: Bubbles</h1>

      <Bubbles flow={flow} />

      <button type="button" onClick={runNext}>
        Run Next
      </button>
    </div>
  );
};

// biome-ignore lint/style/noNonNullAssertion:
createRoot(document.getElementById("root")!).render(<App />);
