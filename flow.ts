type Id = string & { __brand: "Id" };

const randomId = () => {
  const chars = "abcdefghijklmnopqrstuvwxyz0123456789-_.";
  const id = Array.from({ length: 24 })
    .map(() => chars[Math.floor(Math.random() * chars.length)])
    .join("");
  return id as Id;
};

export type RunStatus =
  | { kind: "pending" }
  | { kind: "running"; progress?: number }
  | { kind: "success" }
  | { kind: "failure"; error: unknown };

export type UnitTask<Data> = { kind: "unit"; id: Id; data: Data } & {
  status: RunStatus;
};
export type SeriesTask<Data> = {
  kind: "series";
  id: Id;
  tasks: Task<Data>[];
};
export type ParallelTask<Data> = {
  kind: "parallel";
  id: Id;
  tasks: Task<Data>[];
};
export type CompositeTask<Data> = SeriesTask<Data> | ParallelTask<Data>;
export type Task<Data> = UnitTask<Data> | CompositeTask<Data>;

export const unit = <Data>(value: Data): UnitTask<Data> => ({
  kind: "unit",
  id: randomId(),
  data: value,
  status: { kind: "pending" },
});

export const series = <Data>(...tasks: Task<Data>[]): SeriesTask<Data> => ({
  kind: "series",
  id: randomId(),
  tasks,
});

export const parallel = <Data>(...tasks: Task<Data>[]): ParallelTask<Data> => ({
  kind: "parallel",
  id: randomId(),
  tasks,
});

export class Flow<Data> {
  private taskToParent: Map<Id, Task<Data>> = new Map();
  private allTasks: Task<Data>[] = [];

  constructor(public root: Task<Data>) {
    this._preprocessAll();
  }

  // The following indexes all tasks in the flow, and is used to implement
  // quick lookups for the following methods:
  // - parentOf()
  // - getAllTasks()
  // - getAllUnitTasks()
  // - getAllSeriesTasks()
  // - getAllParallelTasks()
  private _preprocessAll() {
    this.taskToParent.clear();
    this.allTasks = [];
    this._preprocessVisit(this.root, null);
  }

  private _preprocessVisit(task: Task<Data>, parent: Task<Data> | null) {
    this.allTasks.push(task);
    switch (task.kind) {
      case "unit":
        parent && this.taskToParent.set(task.id, parent);
        break;
      case "series":
      case "parallel":
        parent && this.taskToParent.set(task.id, parent);
        for (const child of task.tasks) this._preprocessVisit(child, task);
        break;
    }
  }

  getAllTasks(): readonly Task<Data>[] {
    return this.allTasks;
  }

  getAllUnitTasks(): readonly UnitTask<Data>[] {
    return this.allTasks.filter(
      (task): task is UnitTask<Data> => task.kind === "unit",
    );
  }

  getAllSeriesTasks(): readonly SeriesTask<Data>[] {
    return this.allTasks.filter(
      (task): task is SeriesTask<Data> => task.kind === "series",
    );
  }

  getAllParallelTasks(): readonly ParallelTask<Data>[] {
    return this.allTasks.filter(
      (task): task is ParallelTask<Data> => task.kind === "parallel",
    );
  }

  replace(oldTask: Task<Data>, newTask: Task<Data>) {
    const found = this.allTasks.find((task) => task.id === oldTask.id);
    if (!found) throw new Error("Cannot find task to replace");

    const parent = this.parentOf(oldTask);
    if (!parent) {
      this.root = newTask;
      this._preprocessAll();
      return;
    }

    switch (parent.kind) {
      case "unit":
        throw new Error("Unreachable");

      case "series":
      case "parallel": {
        const idx = this.removeFromComposite(oldTask, parent);
        this.insert(newTask, parent, idx);
      }
    }
  }

  append(task: Task<Data>) {
    switch (this.root.kind) {
      case "unit":
        this.replace(this.root, series(this.root, task));
        break;

      case "series":
      case "parallel": {
        const compositeTask = this.root as CompositeTask<Data>;
        this.insert(task, compositeTask, compositeTask.tasks.length);
        break;
      }
    }
  }

  insert(task: Task<Data>, compositeTask: CompositeTask<Data>, index: number) {
    if (index < 0 || index > compositeTask.tasks.length) {
      throw new Error("Index out of range");
    }
    const tasks = compositeTask.tasks.slice();
    tasks.splice(index, 0, task);
    this._preprocessVisit(task, compositeTask);
  }

  removeFromComposite(task: Task<Data>, parent: CompositeTask<Data>) {
    const idx = parent.tasks.indexOf(task);
    if (idx === -1) throw new Error("Cannot find task to remove");

    const visitOld = (task: Task<Data>) => {
      this.taskToParent.delete(task.id);
      this.allTasks = this.allTasks.filter((t) => t.id !== task.id);
      switch (task.kind) {
        case "series":
        case "parallel":
          for (const child of task.tasks) visitOld(child);
      }
    };
    visitOld(task);

    parent.tasks.splice(idx, 1);
    return idx;
  }

  parentOf(task: Task<Data>) {
    return this.taskToParent.get(task.id);
  }

  childrenOf(task: Task<Data>) {
    switch (task.kind) {
      case "unit":
        return [];
      case "series":
      case "parallel":
        return task.tasks;
    }
  }

  headOf(task: Task<Data>): Task<Data>[] {
    switch (task.kind) {
      case "unit":
        return [task];
      case "series": {
        const head = task.tasks[0];
        return head ? [head] : [];
      }
      case "parallel":
        return task.tasks.flatMap((child) => this.headOf(child));
    }
  }

  isDone(task: Task<Data> = this.root): boolean {
    switch (task.kind) {
      case "unit":
        return task.status.kind === "success";
      case "series":
      case "parallel":
        return task.tasks.every((child) => this.isDone(child));
    }
  }

  immediatelyAfter(
    task: Task<Data>,
    boundaries: CompositeTask<Data>[] = [],
  ): { next: UnitTask<Data>[]; boundaries: CompositeTask<Data>[] } {
    const container = this.parentOf(task);
    if (!container) return { next: [], boundaries: [] };

    switch (container.kind) {
      case "unit":
        throw new Error("Unreachable");
      case "series": {
        const idx = container.tasks.indexOf(task);
        const after = container.tasks.slice(idx + 1);
        if (after.length === 0) {
          return this.immediatelyAfter(container, boundaries.concat(container));
        }
        return {
          // biome-ignore lint/style/noNonNullAssertion:
          next: this.immediatelyNextIn(after[0]!),
          boundaries: boundaries,
        };
      }
      case "parallel": {
        return this.immediatelyAfter(container, boundaries.concat(container));
      }
    }
  }

  immediatelyNextIn(task: Task<Data>): UnitTask<Data>[] {
    switch (task.kind) {
      case "unit":
        return [task];
      case "series": {
        const first = task.tasks[0];
        return first ? this.immediatelyNextIn(first) : [];
      }
      case "parallel":
        return task.tasks.flatMap((child) => this.immediatelyNextIn(child));
    }
  }

  nextReady(): UnitTask<Data>[] {
    const allDoneTasks = this.getAllUnitTasks().filter((task) =>
      this.isDone(task),
    );
    if (allDoneTasks.length === 0) return this.immediatelyNextIn(this.root);

    const nextTasks = allDoneTasks.flatMap((task) => {
      const { next, boundaries } = this.immediatelyAfter(task);
      if (!boundaries.every((task) => this.isDone(task))) {
        return [];
      }
      return next.filter((task) => !this.isDone(task));
    });
    return [...new Set(nextTasks)];
  }

  reportDone(task: UnitTask<Data>): {
    next: UnitTask<Data>[];
    boundaries: CompositeTask<Data>[];
  } {
    task.status = { kind: "success" };
    const { next, boundaries } = this.immediatelyAfter(task);
    if (boundaries.every((task) => this.isDone(task))) {
      return { next, boundaries };
    }
    return { next: [], boundaries: [] };
  }

  async executeOne<R>(
    task: UnitTask<Data>,
    executor: (context: {
      flow: Flow<Data>;
      task: UnitTask<Data>;
      setProgress: (progress: number) => void;
    }) => R | Promise<R>,
  ): Promise<
    | {
        ok: true;
        result: R;
        next: UnitTask<Data>[];
        boundaries: CompositeTask<Data>[];
      }
    | { ok: false; error: unknown }
  > {
    const context = { flow: this, task };
    try {
      task.status = { kind: "running" };
      const result = await executor(
        Object.assign(context, {
          setProgress: (progress: number) => {
            task.status = { kind: "running", progress };
          },
        }),
      );
      const { next, boundaries } = this.reportDone(task);
      return { ok: true, result, next, boundaries };
    } catch (e) {
      task.status = { kind: "failure", error: e };
      return { ok: false, error: e };
    }
  }

  async execute<R>(
    task: UnitTask<Data>,
    executor: (context: {
      flow: Flow<Data>;
      task: UnitTask<Data>;
    }) => R | Promise<R>,
  ): Promise<{ ok: true; results: R[] } | { ok: false; error: unknown }> {
    const executionResult = await this.executeOne(task, executor);
    if (!executionResult.ok) return executionResult;

    const allResults = await Promise.all(
      executionResult.next.map((task) => this.execute(task, executor)),
    );
    const allOkayResults: R[] = [];
    for (const result of allResults) {
      if (!result.ok) return result;
      allOkayResults.push(...result.results);
    }
    return { ok: true, results: allOkayResults };
  }
}
