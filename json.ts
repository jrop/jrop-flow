import { Flow, Task } from "./flow";

// biome-ignore lint/suspicious/noExplicitAny:
export const toJson = (flow: Flow<any>) => {
  return JSON.stringify(flow.root, null, 2);
};

export const fromJson = <Data>(
  json: string,
  isData: (x: unknown) => x is Data,
): Flow<Data> => {
  const isTask = (
    obj: unknown,
    isData: (x: unknown) => x is Data,
  ): obj is Task<Data> => {
    if (!obj || typeof obj !== "object") return false;

    // biome-ignore lint/suspicious/noExplicitAny:
    const id = (obj as any).id;
    if (typeof id !== "string") return false;

    // biome-ignore lint/suspicious/noExplicitAny:
    const kind = (obj as any).kind;

    switch (kind) {
      case "unit": {
        // biome-ignore lint/suspicious/noExplicitAny:
        const status = (obj as any).status;
        if (!status || typeof status !== "object") return false;

        switch (status.kind) {
          case "pending":
          case "success":
          case "running":
          case "failure":
            return true;
          default:
            return false;
        }
      }

      case "series":
      case "parallel": {
        // biome-ignore lint/suspicious/noExplicitAny:
        const tasks = (obj as any).tasks;
        return Array.isArray(tasks) && tasks.every((x) => isTask(x, isData));
      }

      default:
        return false;
    }
  };

  const root = JSON.parse(json) as unknown;
  if (!isTask(root, isData)) throw new Error("Invalid JSON");

  return new Flow(root);
};
