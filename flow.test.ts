import { describe, expect, it } from "bun:test";
import { Flow, Task, parallel, series, unit } from "./flow";

const mkflow = () => {
  //     / 2 \
  // 1 ->     -> 4
  //     \ 3 /
  const _1 = unit(1);
  const _2 = unit(2);
  const _3 = unit(3);
  const _4 = unit(4);
  const par = parallel(_2, _3);
  const ser = series(_1, par, _4);
  return { _1, _2, _3, _4, par, ser, root: ser, flow: new Flow(ser) };
};

const mkbigflow = () => {
  //     / 2 \         / 5 \
  // 1 ->     -> 4 | ->     -> 7
  //     \ 3 /         \ 6 /

  const _1 = unit(1);
  const _2 = unit(2);
  const _3 = unit(3);
  const _4 = unit(4);
  const _5 = unit(5);
  const _6 = unit(6);
  const _7 = unit(7);
  const par1 = parallel(_2, _3);
  const par2 = parallel(_5, _6);
  const ser1 = series(_1, par1, _4);
  const ser2 = series(par2, _7);
  const ser = series(ser1, ser2);

  const flow = new Flow(ser);
  return {
    _1,
    _2,
    _3,
    _4,
    _5,
    _6,
    _7,
    par1,
    par2,
    ser1,
    ser2,
    ser,
    root: ser,
    flow,
  };
};

describe("Flow", () => {
  it(".getAllTasks()", () => {
    const { _1, _2, _3, _4, par, ser, flow } = mkflow();
    const all = flow.getAllTasks();
    expect(all.includes(_1)).toBe(true);
    expect(all.includes(_2)).toBe(true);
    expect(all.includes(_3)).toBe(true);
    expect(all.includes(_4)).toBe(true);
    expect(all.includes(par)).toBe(true);
    expect(all.includes(ser)).toBe(true);
  });
  it(".getAllUnitTasks()", () => {
    const { _1, _2, _3, _4, par, ser, flow } = mkflow();
    const all = flow.getAllUnitTasks();
    expect(all.includes(_1)).toBe(true);
    expect(all.includes(_2)).toBe(true);
    expect(all.includes(_3)).toBe(true);
    expect(all.includes(_4)).toBe(true);
    // biome-ignore lint/suspicious/noExplicitAny:
    expect(all.includes(par as any)).toBe(false);
    // biome-ignore lint/suspicious/noExplicitAny:
    expect(all.includes(ser as any)).toBe(false);
  });
  it(".getAllSeriesTasks()", () => {
    const { _1, _2, _3, _4, par, ser, flow } = mkflow();
    const all = flow.getAllSeriesTasks();
    // biome-ignore lint/suspicious/noExplicitAny:
    expect(all.includes(_1 as any)).toBe(false);
    // biome-ignore lint/suspicious/noExplicitAny:
    expect(all.includes(_2 as any)).toBe(false);
    // biome-ignore lint/suspicious/noExplicitAny:
    expect(all.includes(_3 as any)).toBe(false);
    // biome-ignore lint/suspicious/noExplicitAny:
    expect(all.includes(_4 as any)).toBe(false);
    // biome-ignore lint/suspicious/noExplicitAny:
    expect(all.includes(par as any)).toBe(false);
    expect(all.includes(ser)).toBe(true);
  });
  it(".getAllParallelTasks()", () => {
    const { _1, _2, _3, _4, par, ser, flow } = mkflow();
    const all = flow.getAllParallelTasks();
    // biome-ignore lint/suspicious/noExplicitAny:
    expect(all.includes(_1 as any)).toBe(false);
    // biome-ignore lint/suspicious/noExplicitAny:
    expect(all.includes(_2 as any)).toBe(false);
    // biome-ignore lint/suspicious/noExplicitAny:
    expect(all.includes(_3 as any)).toBe(false);
    // biome-ignore lint/suspicious/noExplicitAny:
    expect(all.includes(_4 as any)).toBe(false);
    expect(all.includes(par)).toBe(true);
    // biome-ignore lint/suspicious/noExplicitAny:
    expect(all.includes(ser as any)).toBe(false);
  });

  it(".replace()", () => {
    const { _1, _4, ser, par, flow } = mkflow();
    const _5 = unit(5);
    flow.replace(par, _5);
    // biome-ignore lint/suspicious/noExplicitAny:
    const taskToParent: Map<string, Task<number>> = (flow as any).taskToParent;
    const mapKeys = [...taskToParent.keys()];
    expect(mapKeys.length).toBe(3);
    expect(mapKeys).toContain(_1.id);
    expect(mapKeys).toContain(_5.id);
    expect(mapKeys).toContain(_4.id);
    expect(taskToParent.get(_5.id)).toBe(ser);

    const allUnitTasks = flow.getAllUnitTasks();
    expect(allUnitTasks.length).toBe(3);
    expect(allUnitTasks).toContain(_1);
    expect(allUnitTasks).toContain(_5);
    expect(allUnitTasks).toContain(_4);
  });

  it(".append()", () => {
    const _1 = unit(1);
    const flow = new Flow(_1);

    const _2 = unit(2);
    flow.append(_2);
    expect(flow.root.kind).toEqual("series");

    const allUnitTasks = flow.getAllUnitTasks();
    expect(allUnitTasks.length).toBe(2);
    expect(allUnitTasks).toContain(_1);
    expect(allUnitTasks).toContain(_2);

    // biome-ignore lint/suspicious/noExplicitAny:
    const taskToParent: Map<string, Task<number>> = (flow as any).taskToParent;
    const mapKeys = [...taskToParent.keys()];
    expect(mapKeys.length).toBe(2);
    expect(mapKeys).toContain(_1.id);
    expect(mapKeys).toContain(_2.id);
  });

  it(".insert()", () => {
    const _1 = unit(1);
    const _3 = unit(3);
    const ser = series(_1, _3);
    const flow = new Flow(ser);

    const _2 = unit(2);
    flow.insert(_2, ser, 1);

    const allUnitTasks = flow.getAllUnitTasks();
    expect(allUnitTasks.length).toBe(3);
    expect(allUnitTasks).toContain(_1);
    expect(allUnitTasks).toContain(_2);
    expect(allUnitTasks).toContain(_3);

    // biome-ignore lint/suspicious/noExplicitAny:
    const taskToParent: Map<string, Task<number>> = (flow as any).taskToParent;
    const mapKeys = [...taskToParent.keys()];
    expect(mapKeys.length).toBe(3);
    expect(mapKeys).toContain(_1.id);
    expect(mapKeys).toContain(_2.id);
    expect(mapKeys).toContain(_3.id);

    expect(flow.parentOf(_1)).toBe(ser);
    expect(flow.parentOf(_2)).toBe(ser);
    expect(flow.parentOf(_3)).toBe(ser);
  });

  it(".removeFromComposite()", () => {
    const _1 = unit(1);
    const _2 = unit(2);
    const _3 = unit(3);
    const ser = series(_1, _2, _3);
    const flow = new Flow(ser);

    flow.removeFromComposite(_2, ser);
    const allUnitTasks = flow.getAllUnitTasks();
    expect(allUnitTasks.length).toBe(2);
    expect(allUnitTasks).toContain(_1);
    expect(allUnitTasks).toContain(_3);

    // biome-ignore lint/suspicious/noExplicitAny:
    const taskToParent: Map<string, Task<number>> = (flow as any).taskToParent;
    const mapKeys = [...taskToParent.keys()];
    expect(mapKeys.length).toBe(2);
    expect(mapKeys).toContain(_1.id);
    expect(mapKeys).toContain(_3.id);

    expect(flow.parentOf(_1)).toBe(ser);
    expect(flow.parentOf(_3)).toBe(ser);
  });

  it(".parentOf(): unit", () => {
    const flow = new Flow(unit(1));
    expect(flow.parentOf(flow.root)).toBe(undefined);
  });
  it(".parentOf(): series", () => {
    const { _1, _2, _3, _4, par, ser, flow } = mkflow();
    expect(flow.parentOf(_1)).toBe(ser);
    expect(flow.parentOf(par)).toBe(ser);
    expect(flow.parentOf(_2)).toBe(par);
    expect(flow.parentOf(_3)).toBe(par);
    expect(flow.parentOf(_4)).toBe(ser);
  });

  it(".childrenOf(): unit", () => {
    const flow = new Flow(unit(1));
    expect(flow.childrenOf(flow.root)).toEqual([]);
  });
  it(".childrenOf(): series", () => {
    const _1 = unit(1);
    const _2 = unit(2);
    const ser = series(_1, _2);
    const flow = new Flow(ser);
    expect(flow.childrenOf(flow.root)).toEqual([_1, _2]);
  });
  it(".childrenOf(): parallel", () => {
    const _1 = unit(1);
    const _2 = unit(2);
    const par = parallel(_1, _2);
    const flow = new Flow(par);
    expect(flow.childrenOf(flow.root)).toEqual([_1, _2]);
  });

  it(".headOf(): unit", () => {
    const _1 = unit(1);
    const flow = new Flow(_1);
    expect(flow.headOf(flow.root)).toEqual([_1]);
  });
  it(".headOf(): series", () => {
    const _1 = unit(1);
    const flow = new Flow(series(_1, parallel(unit(2), unit(3)), unit(4)));
    expect(flow.headOf(flow.root)).toEqual([_1]);
  });
  it(".headOf(): parallel", () => {
    const _2 = unit(2);
    const _3 = unit(3);
    const flow = new Flow(parallel(_2, _3));
    expect(flow.headOf(flow.root)).toEqual([_2, _3]);
  });

  it(".isDone(): unit", () => {
    const _1 = unit(1);
    const flow = new Flow(_1);
    expect(flow.isDone(flow.root)).toBe(false);
    _1.status = { kind: "success" };
    expect(flow.isDone(flow.root)).toBe(true);
  });
  it(".isDone(): series", () => {
    const _1 = unit(1);
    const _2 = unit(2);
    const ser = series(_1, _2);
    const flow = new Flow(ser);
    expect(flow.isDone(ser)).toBe(false);
    _1.status = { kind: "success" };
    expect(flow.isDone(ser)).toBe(false);
    _2.status = { kind: "success" };
    expect(flow.isDone(ser)).toBe(true);
  });
  it(".isDone(): parallel", () => {
    const _1 = unit(1);
    const _2 = unit(2);
    const par = parallel(_1, _2);
    const flow = new Flow(par);
    expect(flow.isDone(par)).toBe(false);
    _1.status = { kind: "success" };
    expect(flow.isDone(par)).toBe(false);
    _2.status = { kind: "success" };
    expect(flow.isDone(par)).toBe(true);
  });

  it(".immediatelyNextIn(): unit", () => {
    const _1 = unit(1);
    const flow = new Flow(_1);
    expect(flow.immediatelyNextIn(flow.root)).toEqual([_1]);
  });
  it(".immediatelyNextIn(): series", () => {
    const _1 = unit(1);
    const flow = new Flow(series(_1, parallel(unit(2), unit(3)), unit(4)));
    expect(flow.immediatelyNextIn(flow.root)).toEqual([_1]);
  });
  it(".immediatelyNextIn(): parallel", () => {
    const _2 = unit(2);
    const _3 = unit(3);
    const flow = new Flow(parallel(_2, _3));
    expect(flow.immediatelyNextIn(flow.root)).toEqual([_2, _3]);
  });

  it(".immediatelyAfter()", () => {
    // little flow:
    {
      const { _1, _2, _3, _4, par, flow } = mkflow();
      expect(flow.immediatelyAfter(_1)).toEqual({
        next: [_2, _3],
        boundaries: [],
      });
      expect(flow.immediatelyAfter(_2)).toEqual({
        next: [_4],
        boundaries: [par],
      });
      expect(flow.immediatelyAfter(_3)).toEqual({
        next: [_4],
        boundaries: [par],
      });
      expect(flow.immediatelyAfter(_4)).toEqual({
        next: [],
        boundaries: [],
      });
    }

    // big flow:
    {
      const { _1, _2, _3, _4, _5, _6, _7, par1, par2, ser1, flow } =
        mkbigflow();
      expect(flow.immediatelyAfter(_1)).toEqual({
        next: [_2, _3],
        boundaries: [],
      });
      expect(flow.immediatelyAfter(_2)).toEqual({
        next: [_4],
        boundaries: [par1],
      });
      expect(flow.immediatelyAfter(_3)).toEqual({
        next: [_4],
        boundaries: [par1],
      });
      expect(flow.immediatelyAfter(_4)).toEqual({
        next: [_5, _6],
        boundaries: [ser1],
      });
      expect(flow.immediatelyAfter(_5)).toEqual({
        next: [_7],
        boundaries: [par2],
      });
      expect(flow.immediatelyAfter(_6)).toEqual({
        next: [_7],
        boundaries: [par2],
      });
      expect(flow.immediatelyAfter(_7)).toEqual({
        next: [],
        boundaries: [],
      });
    }
  });

  it(".nextReady()", () => {
    const { _1, _2, _3, _4, flow } = mkflow();
    expect(flow.nextReady()).toEqual([_1]);
    _1.status = { kind: "success" };
    expect(flow.nextReady()).toEqual([_2, _3]);
    _2.status = { kind: "success" };
    expect(flow.nextReady()).toEqual([_3]);
    _3.status = { kind: "success" };
    expect(flow.nextReady()).toEqual([_4]);
    _4.status = { kind: "success" };
    expect(flow.nextReady()).toEqual([]);
    expect(flow.isDone()).toBe(true);
  });

  it(".reportDone()", () => {
    const { _1, _2, _3, _4, par, flow } = mkflow();
    expect(flow.nextReady()).toEqual([_1]);
    expect(flow.reportDone(_1)).toEqual({
      next: [_2, _3],
      boundaries: [],
    });
    expect(flow.reportDone(_2)).toEqual({
      next: [],
      boundaries: [],
    });
    expect(flow.reportDone(_3)).toEqual({
      next: [_4],
      boundaries: [par],
    });
    expect(flow.reportDone(_4)).toEqual({
      next: [],
      boundaries: [],
    });
    expect(flow.isDone()).toBe(true);
  });
});
