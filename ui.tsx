import { css } from "@emotion/react";
import { Flow, Task, UnitTask } from "./flow";

const Flow_style = css`
  overflow: auto;
  width: 100%;
`;

const Parallel_style = css`
  display: flex;
  flex-direction: column;
`;

const Series_style = css`
  display: flex;
  flex-direction: row;

  & > span {
    display: flex;
    flex-direction: row;
    align-items: center;
  }
`;

export const makeBubblesComponent = <Data,>(opts: {
  unitRenderer: (task: UnitTask<Data> & { ready: boolean }) => JSX.Element;
  separatorRenderer?: (task: Task<Data>) => JSX.Element;
}) => {
  const separatorRenderer = opts.separatorRenderer ?? (() => <>❭</>);

  const TaskView = (props: { task: Task<Data>; readyTasks: Set<string> }) => {
    switch (props.task.kind) {
      case "unit":
        return opts.unitRenderer({
          ...props.task,
          ready: props.readyTasks.has(props.task.id),
        });

      case "parallel": {
        const para = props.task;
        return (
          <div css={Parallel_style}>
            {para.tasks.map((child) => (
              <TaskView
                task={child}
                readyTasks={props.readyTasks}
                key={child.id}
              />
            ))}
          </div>
        );
      }

      case "series": {
        const series = props.task;
        return (
          <div css={Series_style}>
            {series.tasks.map((child, i) => (
              <span key={child.id}>
                <TaskView task={child} readyTasks={props.readyTasks} />
                {i < series.tasks.length - 1 && separatorRenderer(props.task)}
              </span>
            ))}
          </div>
        );
      }
    }
  };

  const Bubbles = (props: { flow: Flow<Data> }) => {
    const readyTasks = new Set(props.flow.nextReady().map((task) => task.id));
    return (
      <div css={Flow_style}>
        <TaskView task={props.flow.root} readyTasks={readyTasks} />
      </div>
    );
  };

  return Bubbles;
};
